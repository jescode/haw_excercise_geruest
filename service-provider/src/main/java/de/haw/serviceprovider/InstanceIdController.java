package de.haw.serviceprovider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//TODO enable RestController
public class InstanceIdController {

    //TODO get the instanceId  via the @Value Annotation
    String instanceId;

    //TODO Add Request Mapping with a endpoint and the http method get
   public String getInstanceId() {
        return instanceId;
    }
}
